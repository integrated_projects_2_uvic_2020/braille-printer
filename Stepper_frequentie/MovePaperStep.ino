void MovePaperStep()
{
  sensorValue1 = analogRead(sensorPin1);    // Between 0 and 1023

  if (sensorValue1 == HIGH)
  {
    // step one revolution  in one direction:
    Serial.println("clockwise");
    myStepper1.step(Numsteps);
    delay(500);
  }

  else
  {
    myStepper1.step(0);
  }
}

