#include <Stepper.h>

const int Numsteps;             // 200 steps per revolution -> 360/200 = 1.8 degrees
int sensorPin1 = A0;
boolean sensorValue1;
int sensorPin2 = A1;
boolean sensorValue2;

// initialize the stepper library on pins 2-5 n 8-11
Stepper myStepper1(Numsteps,2,3,4,5);
Stepper myStepper2(Numsteps,8,9,10,11);   

void setup()
{
 // initialize the serial port:
 Serial.begin(9600);
 
 // set the speed at 120 rpm:
 myStepper1.setSpeed(120);   // Steppermotor 1
 myStepper2.setSpeed(120);   // Steppermotor 2
 
}

void loop()
{
  //For moving the printhead one revolution
  MovePrintheadStep();
  
  //For moving the paper one revolution
  MovePaperStep();
}

